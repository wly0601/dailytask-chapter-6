const {Post} = require("../models")

module.exports = {
	getAllPosts(req,res){
		return Post.findAll()
	},
	postCreate(input){
		return Post.create(input)
	},
	postUpdate(current, input){
		return current.update(input)
	},
	findKey(input){
		return Post.findByPk(input)
	},
	destroy(post){
		return post.destroy()
	}
}
